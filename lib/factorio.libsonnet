{
  _config+:: {
    factorio: {
      name: "factorio",
      image: {
        repo: "docker.io/ofsm/ofsm",
        tag: "0.10.1"
      },
      storageClass: "",
      storageSize: "2Gi",
    },
  },

  local k = import "ksonnet-util/kausal.libsonnet",
  local statefulset = k.apps.v1.statefulSet,
  local container = k.core.v1.container,
  local env = k.core.v1.envVar,
  local port = k.core.v1.containerPort,
  local service = k.core.v1.service,

  local withEnv(name, value) = container.withEnv(
    env.new(name=name, value=value)),

  factorio: {
    deployment: statefulset.new(
      name=$._config.factorio.name
      , replicas=1
      , containers=[
        container.new(
          "factorio"
          , $._config.factorio.image.repo + ":" + $._config.factorio.image.tag
        ) + container.withPorts([port.new("web", 80), port.newUDP("game", 34197)])
        + container.withVolumeMounts([
          k.core.v1.volumeMount.new("data", "/opt/factorio", false),
          k.core.v1.volumeMount.new("fsm-data", "/opt/fsm-data", false),
          k.core.v1.volumeMount.new("fsm-data", "/opt/fsm/mod_packs", false) +
          k.core.v1.volumeMount.withSubPath("mod_packs")
        ])
      ]
    ) + statefulset.spec.withServiceName($.factorio.service.metadata.name)
    + statefulset.spec.withVolumeClaimTemplates([
      {
        metadata: {
          name: "data"
        },
        spec: {
          accessModes: ["ReadWriteOnce"],
          storageClassName: $._config.factorio.storageClass,
          resources: {
            requests: {
              storage: $._config.factorio.storageSize,
            }
          }
        }
      },
      {
        metadata: {
          name: "fsm-data"
        },
        spec: {
          accessModes: ["ReadWriteOnce"],
          storageClassName: $._config.factorio.storageClass,
          resources: {
            requests: {
              storage: '2Gi',
            }
          }
        }
      }
    ]),
    service: k.util.serviceFor(self.deployment) + service.spec.withClusterIP("None"),
  }
}
