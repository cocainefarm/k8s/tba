{
  _config+:: {
    teamspeak: {
      name: 'teamspeak',
      image: {
        repo: 'docker.io/teamspeak',
        tag: '3.13.7',
      },
    },
  },

  local k = import 'ksonnet-util/kausal.libsonnet',
  local statefulset = k.apps.v1.statefulSet,
  local container = k.core.v1.container,
  local env = k.core.v1.envVar,
  local port = k.core.v1.containerPort,
  local service = k.core.v1.service,
  local pvc = k.core.v1.persistentVolumeClaimTemplate,

  local util = import 'gitlab.com/cocainefarm/k8s/lib/util/main.libsonnet',

  local cfg = self._config.teamspeak,

  teamspeak: {
    configmap: k.core.v1.configMap.new(cfg.name, {
      TS3SERVER_DB_CLIENTKEEPDAYS: '30',
      TS3SERVER_DB_CONNECTIONS: '10',
      TS3SERVER_DB_PLUGIN: 'ts3db_sqlite3',
      TS3SERVER_DB_PLUGINPARAMETER: '',
      TS3SERVER_DB_SQLCREATEPATH: '',
      TS3SERVER_DB_SQLPATH: '/opt/ts3server/sql/',
      TS3SERVER_IP_BLACKLIST: '/var/ts3server/query_ip_blacklist.txt',
      TS3SERVER_IP_WHITELIST: '/var/ts3server/query_ip_whitelist.txt',
      TS3SERVER_LICENSE: 'accept',
      TS3SERVER_LICENSEPATH: '/var/ts3server/',
      TS3SERVER_LOG_APPEND: '0',
      TS3SERVER_LOG_PATH: '/var/ts3server/logs/',
      TS3SERVER_LOG_QUERY_COMMANDS: '0',
      TS3SERVER_QUERY_PROTOCOLS: 'ssh',
      TS3SERVER_QUERY_SSH_RSA_HOST_KEY: '/var/ts3server/host.key',
      TS3SERVER_QUERY_TIMEOUT: '300',
    }),
    pvc:: util.volumeClaimTemplate.new('data', '2Gi'),
    deployment: statefulset.new(
                  name=cfg.name
                  , replicas=1
                  , containers=[
                    container.new(
                      'teamspeak'
                      , cfg.image.repo + ':' + cfg.image.tag
                    ) + container.withPorts([
                      port.new('voice', 9987),
                      port.new('query', 10022),
                      port.new('files', 30033),
                    ])
                    + container.withVolumeMounts([
                      k.core.v1.volumeMount.new('data', '/var/ts3server', false),
                    ])
                    + container.withEnvFrom([
                      k.core.v1.envFromSource.configMapRef.withName(self.configmap.metadata.name),
                    ])
                    + util.tcpProbes('query', 15, 5, 10, 5),
                  ]
                ) + statefulset.spec.withServiceName(self.serviceHeadless.metadata.name)
                + statefulset.hostVolumeMount('shm', '/dev/shm', '/dev/shm', false, {})
                + statefulset.spec.withVolumeClaimTemplates([self.pvc]),
    serviceHeadless: util.serviceFor(self.deployment) +
                     service.spec.withClusterIP('None') +
                     service.metadata.withName('%s-headless' % cfg.name),
  },
}
