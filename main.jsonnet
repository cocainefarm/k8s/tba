local k = import 'ksonnet-util/kausal.libsonnet';
local util = import 'gitlab.com/cocainefarm/k8s/lib/util/main.libsonnet';

local service = k.core.v1.service;

function(tag='', namespace='tba', envSlug=null, projectPathSlug=null)
  (util.inlineSpec('https://ettves.vapor.systems:6443', namespace, envSlug, projectPathSlug))
  + {
    data: {
      teamspeak: (import 'teamspeak.libsonnet') + {
        teamspeak+: {
          ext_service: k.util.serviceFor(self.deployment)
                      + k.core.v1.service.spec.withExternalIPs(['178.63.224.12'])
                      + k.core.v1.service.metadata.withName('teamspeak-ext'),
        }
      },
      factorio: (import 'factorio.libsonnet') + {
        _config+: {
          factorio+: {
            storageClass: 'ssd',
          },
        },

        factorio+: {
          ext_service: k.util.serviceFor(self.deployment)
                      + k.core.v1.service.spec.withExternalIPs(['178.63.224.12'])
                      + k.core.v1.service.metadata.withName('factorio-ext'),
          ingress: util.ingressFor(self.service, 'factorio.cocaine.farm', 'cocaine-farm-tls'),
        },
      },
    }
  }
